// Comprobación en campos de entrada
function checkVacios(busqueda, fechaMin, fechaMax){
    if (busqueda=="" || fechaMin=="" || fechaMax=="") {
        return true;
    }
    return false;
}
function checkLong(fechaMin, fechaMax){
    if (fechaMin.length!=8) {
        return true;
    }
    if (fechaMax.length!=8) {
        return true;
    }
    return false;
}
function checkFechas(fechaMin, fechaMax){
    if (fechaMin>fechaMax) {
        return true;
    }
    return false;
}
//////////////////////////////////////////////////////////////////////////////////////////
// Definimos el código para procesar la respuesta que pasaremos como parametro al objeto
function procesaRespuesta(result) {
    var cadena="";
    $("#capaSalida").html("");
    for (i=0;i<result.response.docs.length;i++){
        cadena+="<li>"+result.response.docs[i].pub_date+" : "+result.response.docs[i].lead_paragraph+ "</li>";
    }    
    $("#capaSalida").html("<ul>"+cadena+"</ul>");
}

function obtenerDatos()
{
    var busqueda = $("#busqueda").val();
    var fechaMin = $("#fechaMin").val();
    var fechaMax = $("#fechaMax").val();
    var selectOrden = $("#selectOrden").val();

    var hayVacios=checkVacios(busqueda, fechaMin, fechaMax);
    var longError=checkLong(fechaMin, fechaMax);
    var fechasError=checkFechas(fechaMin, fechaMax);
    
    if(hayVacios==false){
        if (longError==false) {
            if (fechasError==false) {
                var url = "https://api.nytimes.com/svc/search/v2/articlesearch.json";
                url += '?' + $.param({
                    'api-key': "7dc8d61dbd414faeab82d6b90b36314b",
                    'q': busqueda,
                    'begin_date': fechaMin,
                    'end_date': fechaMax,
                    'sort': selectOrden
                });
                $.ajax({
                    url: url,
                    method: 'GET',
                }).done(function(result) {
                    console.log(result);
                    procesaRespuesta(result);
                }).fail(function(err) {
                    throw err;
                });
            } else{
                $("#capaSalida").html("La fecha máxima debes ser mayor que la fecha mínima.");
            }
        } else{
            $("#capaSalida").html("Las fechas tienen deben cumplir con el siguiente formato YYYYMMDD.");
        }
    } else{
        $("#capaSalida").html("Todos los campos son requeridos.");
    }
}

// Al carga la pagina asociamos al onclick del boton el método
$(document).ready(function(){
    $("#buscar").click(obtenerDatos);
})
